-- Leads by Lead Source
-- This report presents number of leads broken down by Lead Source
-- VARIABLE: { 
--      name: "range", 
--      display: "Date Range",
--      type: "daterange", 
--      default: { start: "yesterday", end: "today" }
-- }
-- CHART: {
--      "columns": ["Lead Source", "Leads"],
--      "type": "BarChart",
--      "title": "Leads by Lead Source",
--      "width": "100%",
--      "height": "1200px",
--      "options": {
--          "hAxis": {
--              "title": 'Number of Leads'
--          },
--          "vAxis": {
--              "title": 'Lead Source'
--          },
--      }
-- }

SELECT l.LeadSource as 'Lead Source', COUNT(*) as 'Leads'
FROM Lead AS l
WHERE l.CreateDate BETWEEN "{{ range.start }}" AND "{{ range.end }}"
GROUP BY l.LeadSource
ORDER BY l.LeadSource;