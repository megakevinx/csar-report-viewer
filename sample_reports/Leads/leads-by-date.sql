-- Leads by Date
-- This report presents number of leads broken down by Date
-- VARIABLE: { 
--      name: "range", 
--      display: "Date Range",
--      type: "daterange", 
--      default: { start: "yesterday", end: "today" }
-- }
-- CHART: {
--      "columns": ["Date", "Leads"],
--      "type": "LineChart",
--      "title": "Leads by Date",
--      "width": "100%",
--      "height": "800px",
--      "options": {
--          "vAxis": {
--              "title": 'Number of Leads'
--          },
--      }
-- }

SELECT DATE(l.CreateDate) AS 'Date', COUNT(*) AS 'Leads'
FROM Lead AS l
WHERE l.CreateDate BETWEEN "{{ range.start }}" AND "{{ range.end }}"
GROUP BY DATE(l.CreateDate);