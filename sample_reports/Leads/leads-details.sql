-- Leads Details
-- This report presents detailed information about all of the captured leads
-- VARIABLE: { 
--      name: "range", 
--      display: "Date Range",
--      type: "daterange", 
--      default: { start: "yesterday", end: "today" }
-- }

SELECT DATE(l.CreateDate) AS 'Date Captured',
	l.VehicleYear AS 'Year', l.VehicleMake AS 'Make', l.VehicleModel AS 'Model', 
    IF(l.VehicleHasTitle IS NULL, NULL, IF(l.VehicleHasTitle = 1, 'Yes', 'No')) AS 'Has Title',
    IF(l.VehicleHasFlatTire IS NULL, NULL, IF(l.VehicleHasFlatTire = 1, 'Yes', 'No')) AS 'Has Flat Tire',
    IF(l.VehicleIsAwd IS NULL, NULL, IF(l.VehicleIsAwd = 1, 'Yes', 'No')) AS 'Is Awd',
	l.LeadSource AS 'Lead Source', l.LeadStatus AS 'Lead Status',
    l.SellerName AS 'Seller Name', l.SellerPhoneNumber AS 'Seller Phone Number', l.SellerEmail AS 'Seller Email',
    l.VehicleLocationType AS 'Location Type', l.VehicleStreetAddress AS 'Street Address', l.VehicleCity AS 'City', l.VehicleZipCode AS 'Zip Code', 
    l.VehiclePickupDate AS 'Pickup Date', l.VehiclePickupWindowFrom AS 'Pickup From', l.VehiclePickupWindowTo AS 'Pickup To'
FROM Lead AS l
WHERE l.CreateDate BETWEEN "{{ range.start }}" AND "{{ range.end }}"
ORDER BY l.Lead_ID;