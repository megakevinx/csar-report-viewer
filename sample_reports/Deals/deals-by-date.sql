-- Deals by Date
-- This report presents number of deals broken down by Date
-- VARIABLE: { 
--      name: "range", 
--      display: "Date Range",
--      type: "daterange", 
--      default: { start: "yesterday", end: "today" }
-- }
-- CHART: {
--      "columns": ["Date", "Deals"],
--      "type": "LineChart",
--      "title": "Deals by Date",
--      "width": "100%",
--      "height": "800px",
--      "options": {
--          "vAxis": {
--              "title": 'Number of Deals'
--          },
--      }
-- }

SELECT i.DateVal AS 'Date', COUNT(*) AS 'Deals'
FROM Inspection AS i
WHERE i.Cancelled = 0 AND i.Incomplete != 1 AND i.DateVal BETWEEN "{{ range.start }}" AND "{{ range.end }}"
GROUP BY i.DateVal;