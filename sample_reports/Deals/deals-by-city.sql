-- Deals by City
-- This report presents number of deals broken down by City
-- VARIABLE: { 
--      name: "range", 
--      display: "Date Range",
--      type: "daterange", 
--      default: { start: "yesterday", end: "today" }
-- }
-- CHART: {
--      "columns": ["City", "Deals"],
--      "type": "BarChart",
--      "title": "Deals by City",
--      "width": "100%",
--      "height": "1200px",
--      "options": {
--          "hAxis": {
--              "title": 'Number of Deals'
--          },
--          "vAxis": {
--              "title": 'City'
--          },
--      }
-- }

SELECT
    IFNULL(zc.City, 'N/A') AS 'City',
    (
        SELECT GROUP_CONCAT(izc.ZipCode SEPARATOR ', ')
        FROM Zip_City AS izc
        WHERE izc.City = zc.City
    ) AS 'Zip Codes',
    Count(*) as 'Deals'
FROM Inspection AS i
LEFT OUTER JOIN Zip_City AS zc ON i.Zip = zc.ZipCode COLLATE utf8_unicode_ci
WHERE i.Cancelled = 0 AND i.Incomplete != 1 AND i.DateVal BETWEEN "{{ range.start }}" AND "{{ range.end }}"
GROUP BY zc.City, 'Zip Codes'
ORDER BY zc.City;