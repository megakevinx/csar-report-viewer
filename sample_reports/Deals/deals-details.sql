-- Deals Details
-- This report presents detailed information about all of the captured deals
-- VARIABLE: { 
--      name: "range", 
--      display: "Date Range",
--      type: "daterange", 
--      default: { start: "yesterday", end: "today" }
-- }

SELECT
    Inspection_ID AS 'Deal #',
    DateVal AS 'Deal Date',
    _Date AS 'Date of Purchase',
    LeadSource AS 'Source/3rd Party Buyer',
    _Year AS 'Year',
    Make AS 'Make',
    Model AS 'Model',
    Color AS 'Color',
    VIN AS 'VIN',
    KeysLoc AS 'Keys',
    Wheels AS 'Wheels',
    Doors AS 'Doors',
    TitleNum AS 'Title #',
    Title AS 'Has Title',
    DatePmtRecd AS 'Paid Date',
    PmtRecd AS 'Paid (Y/N)',
    PurchAmt AS 'Purchase Price Customer',
    LeadCost AS 'Purchase Price 3rd Party',
    BuyerName AS 'Field Agent',
    BuyerPay AS 'Field Agent Pay',
    COALESCE((SELECT SUM(ts.Amount) FROM TowStatus AS ts WHERE ts.Inspection_ID = Inspection.Inspection_ID), 0) AS 'Tow Cost',
    DMV_Fees AS 'Fees (DTs)',
    TowID AS 'Tow ID',
    EndBuyer AS 'Buyer',
    SellPrice AS 'Sell Price',
    COALESCE((SELECT SUM(ivc.TotalPrice) FROM Inspection_VehicleCore AS ivc WHERE ivc.Inspection_ID = Inspection.Inspection_ID), 0) AS 'Cores Value',

    (
        (
            SellPrice +
            COALESCE((SELECT SUM(ivc.TotalPrice) FROM Inspection_VehicleCore AS ivc WHERE ivc.Inspection_ID = Inspection.Inspection_ID), 0)
        ) -
        (
            PurchAmt +
            LeadCost +
            BuyerPay +
            COALESCE((SELECT SUM(ts.Amount) FROM TowStatus AS ts WHERE ts.Inspection_ID = Inspection.Inspection_ID), 0) +
            DMV_Fees
        )
    ) AS 'N.O.I.',

    (
        CASE WHEN (SellPrice > 0 || COALESCE((SELECT SUM(ivc.TotalPrice) FROM Inspection_VehicleCore AS ivc WHERE ivc.Inspection_ID = Inspection.Inspection_ID), 0) > 0)
            THEN
                FORMAT(
                    (
                        (
                            (
                                SellPrice +
                                COALESCE((SELECT SUM(ivc.TotalPrice) FROM Inspection_VehicleCore AS ivc WHERE ivc.Inspection_ID = Inspection.Inspection_ID), 0)
                            ) -
                            (
                                PurchAmt +
                                LeadCost +
                                BuyerPay +
                                COALESCE((SELECT SUM(ts.Amount) FROM TowStatus AS ts WHERE ts.Inspection_ID = Inspection.Inspection_ID), 0) +
                                DMV_Fees
                            )
                        ) /
                        (
                            SellPrice +
                            COALESCE((SELECT SUM(ivc.TotalPrice) FROM Inspection_VehicleCore AS ivc WHERE ivc.Inspection_ID = Inspection.Inspection_ID), 0)
                        )
                    ),
                    2
                )
            ELSE
                NOI_pct
        END
    ) AS 'N.O.I. %',

    -- NOI AS 'N.O.I.',

    -- (
    --     (
    --         SellPrice +
    --         COALESCE((SELECT SUM(ivc.TotalPrice) FROM Inspection_VehicleCore AS ivc WHERE ivc.Inspection_ID = Inspection.Inspection_ID), 0)
    --     ) -
    --     (
    --         PurchAmt +
    --         LeadCost +
    --         BuyerPay +
    --         COALESCE((SELECT SUM(ts.Amount) FROM TowStatus AS ts WHERE ts.Inspection_ID = Inspection.Inspection_ID), 0) +
    --         DMV_Fees
    --     )
    -- ) AS 'N.O.I. %',

    -- NOI_pct AS 'N.O.I. %',

    SellerName AS 'Customer Name',
    Phone AS 'Customer Phone',
    FullAddress AS 'Customer Address',
    SellerID AS 'I.D.',
    PickupNotes AS 'Pickup Notes',
    Notes AS 'Misc. Notes'
FROM Inspection
INNER JOIN Buyer ON Inspection.Buyer_ID = Buyer.Buyer_ID
WHERE DateVal BETWEEN "{{ range.start }}" AND "{{ range.end }}"
AND Cancelled = 0 AND Incomplete = 0
ORDER BY DateVal, Inspection_ID
