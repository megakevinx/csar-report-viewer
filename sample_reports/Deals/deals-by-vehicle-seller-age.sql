-- Deals by Seller Age
-- This report presents number of deals broken down by Seller Age
-- VARIABLE: { 
--      name: "range", 
--      display: "Date Range",
--      type: "daterange", 
--      default: { start: "yesterday", end: "today" }
-- }
-- CHART: {
--      "columns": ["Seller Age", "Deals"],
--      "type": "ColumnChart",
--      "title": "Deals by Seller Age",
--      "width": "100%",
--      "height": "800px",
--      "options": {
--          "hAxis": {
--              "title": 'Seller Age'
--          },
--          "vAxis": {
--              "title": 'Number of Deals'
--          },
--      }
-- }

SELECT
    CASE
        WHEN CAST(SUBSTRING(i.SellerID, 8, 2) AS SIGNED) < CONVERT(SUBSTRING(CONVERT(YEAR(CURDATE()), CHAR(4)), 3, 2), SIGNED) THEN
            TIMESTAMPDIFF(YEAR, CONCAT("20", SUBSTRING(i.SellerID, 8, 2), "-01-01"), CURDATE())
        ELSE
            TIMESTAMPDIFF(YEAR, CONCAT("19", SUBSTRING(i.SellerID, 8, 2), "-01-01"), CURDATE())
    END AS 'Seller Age',
    COUNT(*) AS 'Deals'
FROM Inspection AS i
WHERE
    i.Cancelled = 0 AND i.Incomplete != 1
    AND CHAR_LENGTH(i.SellerID) = 13
    AND i.DateVal BETWEEN "{{ range.start }}" AND "{{ range.end }}"
GROUP BY
    CASE
        WHEN CAST(SUBSTRING(i.SellerID, 8, 2) AS SIGNED) < CONVERT(SUBSTRING(CONVERT(YEAR(CURDATE()), CHAR(4)), 3, 2), SIGNED) THEN
            TIMESTAMPDIFF(YEAR, CONCAT("20", SUBSTRING(i.SellerID, 8, 2), "-01-01"), CURDATE())
        ELSE
            TIMESTAMPDIFF(YEAR, CONCAT("19", SUBSTRING(i.SellerID, 8, 2), "-01-01"), CURDATE())
    END;
