-- Deals by Lead Source
-- This report presents number of deals broken down by Lead Source
-- VARIABLE: { 
--      name: "range", 
--      display: "Date Range",
--      type: "daterange", 
--      default: { start: "yesterday", end: "today" }
-- }
-- CHART: {
--      "columns": ["Lead Source", "Deals"],
--      "type": "BarChart",
--      "title": "Deals by Lead Source",
--      "width": "100%",
--      "height": "1200px",
--      "options": {
--          "hAxis": {
--              "title": 'Number of Deals'
--          },
--          "vAxis": {
--              "title": 'Lead Source'
--          },
--      }
-- }

SELECT
    IF(i.LeadSource IS NULL or i.LeadSource = '', 'N/A', i.LeadSource) as 'Lead Source',
    Count(*) as 'Deals'
FROM Inspection AS i
WHERE i.Cancelled = 0 AND i.Incomplete != 1 AND i.DateVal BETWEEN "{{ range.start }}" AND "{{ range.end }}"
GROUP BY i.LeadSource
ORDER BY i.LeadSource;