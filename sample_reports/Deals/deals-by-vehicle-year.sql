-- Deals by Vehicle Year
-- This report presents number of deals broken down by Vehicle Year
-- VARIABLE: { 
--      name: "range", 
--      display: "Date Range",
--      type: "daterange", 
--      default: { start: "yesterday", end: "today" }
-- }
-- CHART: {
--      "columns": ["Vehicle Year", "Deals"],
--      "type": "ColumnChart",
--      "title": "Deals by Vehicle Year",
--      "width": "100%",
--      "height": "800px",
--      "options": {
--          "hAxis": {
--              "title": 'Vehicle Year',
--              format: ''
--          },
--          "vAxis": {
--              "title": 'Number of Deals'
--          },
--      }
-- }

SELECT i._Year AS 'Vehicle Year', COUNT(*) AS 'Deals'
FROM Inspection AS i 
WHERE i.Cancelled = 0 AND i.Incomplete != 1 AND i.DateVal BETWEEN "{{ range.start }}" AND "{{ range.end }}"
GROUP BY i._Year