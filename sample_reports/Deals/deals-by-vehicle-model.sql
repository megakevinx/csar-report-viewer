-- Deals by Vehicle Model
-- This report presents number of deals broken down by Vehicle Model
-- VARIABLE: { 
--      name: "range", 
--      display: "Date Range",
--      type: "daterange", 
--      default: { start: "yesterday", end: "today" }
-- }
-- CHART: {
--      "columns": ["Vehicle Model", "Deals"],
--      "type": "BarChart",
--      "title": "Deals by Vehicle Model",
--      "width": "100%",
--      "height": "800px",
--      "options": {
--          "hAxis": {
--              "title": 'Number of Deals'
--          },
--          "vAxis": {
--              "title": 'Vehicle Model'
--          },
--      }
-- }

SELECT CONCAT(i.Make, ' ', i.Model) AS 'Vehicle Model', COUNT(*) AS 'Deals'
FROM Inspection AS i 
WHERE i.Cancelled = 0 AND i.Incomplete != 1 AND i.DateVal BETWEEN "{{ range.start }}" AND "{{ range.end }}"
GROUP BY CONCAT(i.Make, ' ', i.Model)
ORDER BY CONCAT(i.Make, ' ', i.Model)
