-- Deals by Seller Gender
-- This report presents number of deals broken down by Seller Gender
-- VARIABLE: { 
--      name: "range", 
--      display: "Date Range",
--      type: "daterange", 
--      default: { start: "yesterday", end: "today" }
-- }
-- CHART: {
--      "columns": ["Seller Gender", "Deals"],
--      "type": "ColumnChart",
--      "title": "Deals by Seller Gender",
--      "width": "100%",
--      "height": "800px",
--      "options": {
--          "hAxis": {
--              "title": 'Seller Gender',
--              format: ''
--          },
--          "vAxis": {
--              "title": 'Number of Deals'
--          },
--      }
-- }

SELECT IFNULL(i.SellerGender, 'N/A') AS 'Seller Gender', COUNT(*) AS 'Deals'
FROM Inspection AS i 
WHERE i.Cancelled = 0 AND i.Incomplete != 1 AND i.DateVal BETWEEN "{{ range.start }}" AND "{{ range.end }}"
GROUP BY i.SellerGender;